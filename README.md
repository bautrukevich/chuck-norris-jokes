# Chuck Norris Jokes

Create a Chuck Norris jokes in your next PHP project.

## Installation

Require the package using composer:

```bash
composer require bautrukevich/chuck-norris-jokes
```

## Usage

```php
use Bautrukevich\ChuckNorrisJokes\JokeFactory;

$factory = new JokeFactory();
$joke = $factory->getRandomJoke();
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](./LICENSE)